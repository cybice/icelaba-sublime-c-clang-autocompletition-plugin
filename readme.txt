C++ Clang autocompletition small plugin

Add this line to Menu/Preferences/Settings User
"auto_complete_triggers": [ {"selector": "source.c++", "characters": "~"} ]
at 
~/.config/sublime-text-2/Packages/User/Preferences.sublime-settings

to view autocompletition just type
.~
or 
->~
