#(c)icelaba aka istarkov@gmail.com
#YOU CAN DO ANYTHING WITH THIS CODE,
#SO LONG AS YOU DON'T BREAK THE LAW

import sublime, sublime_plugin
import subprocess
import os

class IceAutocomplete(sublime_plugin.EventListener):
    #place here path to any header lib pathes you need -I/path1 -I/path2 -I/path3 etc...
    USER_HEADER_INCLUDE_PATH = "" #"-I/path" 

    def on_query_completions(self, view, prefix, locations):
        dirname = os.path.dirname(view.file_name())
        (row,col) = view.rowcol(view.sel()[0].begin())
        lineContents = view.substr(view.line(view.sel()[0]))

        if (lineContents.find('.~'+prefix)>-1 or lineContents.find('->~'+prefix)>-1) and len(prefix)==0:

          region = sublime.Region(view.sel()[0].begin()-1, view.sel()[0].end())
          edit = view.begin_edit()
          view.replace(edit, region, "");
          view.end_edit(edit)
          
          #see cat {$cpp_path} | clang -cc1 -x c++ -fsyntax-only -fcxx-exceptions -I{$cppfile_dir} -code-completion-at -:{$row}:{$col} -
          process = subprocess.Popen( 
                "clang -cc1 -x c++ -fsyntax-only -fcxx-exceptions %s -I%s -code-completion-at -:%d:%d -"%(self.USER_HEADER_INCLUDE_PATH, dirname, row+1, col) 
              , shell   = True
              , stdin   = subprocess.PIPE
              , stdout  = subprocess.PIPE
              , stderr  = subprocess.PIPE
              )
          
          sdatain = view.substr(sublime.Region(0,view.size()))
          result, error = process.communicate(input = sdatain.encode('utf8'))
          arr = [par[12:].replace('::','$##$').split(':') for par in result.split("\n") if par.startswith('COMPLETION')] 
          arr2 = [((''+a[1]).replace('$##$', '::'), a[0]) for a in arr if len(a) > 1]
          return arr2
        return []